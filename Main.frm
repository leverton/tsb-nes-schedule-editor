VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TSB Schedule Editor"
   ClientHeight    =   4800
   ClientLeft      =   6630
   ClientTop       =   5235
   ClientWidth     =   6990
   Icon            =   "Main.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4800
   ScaleWidth      =   6990
   Begin MSComDlg.CommonDialog cdOpen 
      Left            =   6240
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame frmContent 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   4455
      Left            =   120
      TabIndex        =   1
      Top             =   0
      Width           =   6735
      Begin VB.Frame Frame1 
         Caption         =   "Matchups"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   6495
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   27
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   30
            Text            =   "cmbTeam"
            Top             =   3120
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   26
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   29
            Text            =   "cmbTeam"
            Top             =   3120
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   25
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   28
            Text            =   "cmbTeam"
            Top             =   2640
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   24
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   27
            Text            =   "cmbTeam"
            Top             =   2640
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   23
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   26
            Text            =   "cmbTeam"
            Top             =   2160
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   22
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   25
            Text            =   "cmbTeam"
            Top             =   2160
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   21
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   24
            Text            =   "cmbTeam"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   20
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   23
            Text            =   "cmbTeam"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   19
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   22
            Text            =   "cmbTeam"
            Top             =   1200
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   18
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   21
            Text            =   "cmbTeam"
            Top             =   1200
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   17
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   20
            Text            =   "cmbTeam"
            Top             =   720
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   16
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   19
            Text            =   "cmbTeam"
            Top             =   720
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   15
            Left            =   5160
            Sorted          =   -1  'True
            TabIndex        =   18
            Text            =   "cmbTeam"
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   14
            Left            =   3480
            Sorted          =   -1  'True
            TabIndex        =   17
            Text            =   "cmbTeam"
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   13
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   16
            Text            =   "cmbTeam"
            Top             =   3120
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   12
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   15
            Text            =   "cmbTeam"
            Top             =   3120
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   11
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   14
            Text            =   "cmbTeam"
            Top             =   2640
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   10
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   13
            Text            =   "cmbTeam"
            Top             =   2640
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   9
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   12
            Text            =   "cmbTeam"
            Top             =   2160
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   8
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   11
            Text            =   "cmbTeam"
            Top             =   2160
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   7
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   10
            Text            =   "cmbTeam"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   6
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   9
            Text            =   "cmbTeam"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   5
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   8
            Text            =   "cmbTeam"
            Top             =   1200
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   4
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   7
            Text            =   "cmbTeam"
            Top             =   1200
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   3
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   6
            Text            =   "cmbTeam"
            Top             =   720
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   2
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   5
            Text            =   "cmbTeam"
            Top             =   720
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   1
            Left            =   1800
            Sorted          =   -1  'True
            TabIndex        =   4
            Text            =   "cmbTeam"
            Top             =   240
            Width           =   1215
         End
         Begin VB.ComboBox cmbTeam 
            Height          =   315
            Index           =   0
            Left            =   120
            Sorted          =   -1  'True
            TabIndex        =   3
            Text            =   "cmbTeam"
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   1440
            TabIndex        =   44
            Top             =   240
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   1440
            TabIndex        =   43
            Top             =   720
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   1440
            TabIndex        =   42
            Top             =   1200
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   1440
            TabIndex        =   41
            Top             =   1680
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   1440
            TabIndex        =   40
            Top             =   2160
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   1440
            TabIndex        =   39
            Top             =   2640
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   6
            Left            =   1440
            TabIndex        =   38
            Top             =   3120
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   7
            Left            =   4800
            TabIndex        =   37
            Top             =   240
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   8
            Left            =   4800
            TabIndex        =   36
            Top             =   720
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   9
            Left            =   4800
            TabIndex        =   35
            Top             =   1200
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   10
            Left            =   4800
            TabIndex        =   34
            Top             =   1680
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   11
            Left            =   4800
            TabIndex        =   33
            Top             =   2160
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   12
            Left            =   4800
            TabIndex        =   32
            Top             =   2640
            Width           =   210
         End
         Begin VB.Label lblAt 
            AutoSize        =   -1  'True
            Caption         =   "@"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   13
            Left            =   4800
            TabIndex        =   31
            Top             =   3120
            Width           =   210
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000010&
            X1              =   3240
            X2              =   3240
            Y1              =   360
            Y2              =   3240
         End
      End
      Begin MSComCtl2.UpDown udWeek 
         Height          =   255
         Left            =   1680
         TabIndex        =   45
         Top             =   285
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   450
         _Version        =   393216
         Alignment       =   0
         Orientation     =   1
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown upGames 
         Height          =   255
         Left            =   4920
         TabIndex        =   46
         Top             =   285
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   450
         _Version        =   393216
         Alignment       =   0
         Orientation     =   1
         Enabled         =   -1  'True
      End
      Begin VB.Label lblWeek 
         Caption         =   "Week:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   48
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lblGames 
         Caption         =   "Games:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   47
         Top             =   240
         Width           =   1335
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   4545
      Width           =   6990
      _ExtentX        =   12330
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12277
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open..."
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuFileB1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim tsbFile As Integer
Dim tsbFileName As String

Private Type CTeam
    Abbr As String
End Type

Private Type CMatchup
    Home As Integer
    Visitor As Integer
End Type

Private Type CWeek
    Matchup(0 To 13) As CMatchup
    gameCount As Byte
End Type

Private Type CSchedule
    Week(0 To 16) As CWeek
End Type

Dim Schedule As CSchedule
Dim Team(0 To 29) As CTeam

Dim weekNumber As Byte

Dim loadingWeek As Boolean

Private Sub cmbTeam_Click(Index As Integer)
    If loadingWeek Then Exit Sub
    
    If cmbTeam(Index).ListIndex = -1 Then
        If cmbTeam(Index).Text <> "" Then
            Dim i As Integer, j As Integer
            For i = 0 To cmbTeam(Index).ListCount - 1
                If UCase(Left(cmbTeam(Index).List(i), Len(cmbTeam(Index).Text))) = UCase(cmbTeam(Index).Text) Then
                    cmbTeam(Index).ListIndex = i
                    
                    For j = 0 To 29
                        If Team(j).Abbr = cmbTeam(Index).Text Then
                            If (Index Mod 2) = 1 Then
                                Schedule.Week(weekNumber).Matchup(Index \ 2).Home = j
                            Else
                                Schedule.Week(weekNumber).Matchup(Index \ 2).Visitor = j
                            End If
                            Exit For
                        End If
                    Next j
                    
                    Exit Sub
                End If
            Next
        
            cmbTeam(Index).Text = ""
        End If
    Else
        For j = 0 To 29
            If Team(j).Abbr = cmbTeam(Index).Text Then
                If (Index Mod 2) = 1 Then
                    Schedule.Week(weekNumber).Matchup(Index \ 2).Home = j
                Else
                    Schedule.Week(weekNumber).Matchup(Index \ 2).Visitor = j
                End If
                Exit For
            End If
        Next j
    End If
End Sub

Private Sub cmbTeam_LostFocus(Index As Integer)
    Call cmbTeam_Click(Index)
End Sub

Private Sub Form_Load()
    frmContent.Visible = False
    StatusBar.Panels(1).Text = "Load a TSB NES ROM to begin."
    
End Sub

Public Function readUINT16(fp As Integer, Optional address As Long) As Long
    Dim INT16 As Integer
    
        
    ' VB starts files at 1
    If address > 0 Then
        Get fp, address + 1, INT16
    Else
        Get fp, , INT16
    End If
    If INT16 < 0 Then readUINT16 = INT16 + &H10000 Else readUINT16 = INT16
    
End Function

Public Function readINT8(fp As Integer, Optional address As Long) As Byte
    Dim INT8 As Byte
    
    ' VB starts files at 1
    If address > 0 Then
        Get fp, address + 1, INT8
    Else
        Get fp, , INT8
    End If
    
    readINT8 = INT8
End Function

Public Function readString(fp As Integer, length As Integer) As String
    Dim buffer As String
    buffer = Space(length)
    
    Get #fp, , buffer
    readString = buffer
End Function

Private Sub Label2_Click()

End Sub

Public Sub LoadWeek(weekNum As Byte)
    Dim i As Integer, j As Integer
    
    loadingWeek = True
    
    lblWeek.Caption = "Week: " & (weekNum + 1)
        
    For i = 0 To 27
        cmbTeam(i).ListIndex = 0
        cmbTeam(i).Text = ""
    Next i
    
    Call showMatchups
    
    For i = 0 To Schedule.Week(weekNum).gameCount - 1
        For j = 0 To 27
            If cmbTeam(i * 2).List(j) = Team(Schedule.Week(weekNum).Matchup(i).Visitor).Abbr Then
                cmbTeam(i * 2).ListIndex = j
                Exit For
            End If
        Next j
        
        For j = 0 To 27
            If cmbTeam(i * 2 + 1).List(j) = Team(Schedule.Week(weekNum).Matchup(i).Home).Abbr Then
                cmbTeam(i * 2 + 1).ListIndex = j
                Exit For
            End If
        Next j
    Next i
    
    loadingWeek = False
End Sub

Private Sub Label1_Click()

End Sub

Private Sub mnuFileExit_Click()
    Unload Me
End Sub

Private Sub UpDown1_Change()

End Sub

Private Sub mnuFileOpen_Click()
    On Error GoTo errorHandler
    
    With cdOpen
        cdOpen.Filter = "NES ROMs (*.nes)|*.nes|All Files|*.*"
        cdOpen.Flags = 4
        cdOpen.CancelError = True
        cdOpen.ShowOpen
        
        Call openFile(cdOpen.fileName)
        mnuFileSave.Enabled = True
        tsbFileName = cdOpen.fileName
    End With
    
    Exit Sub
    
errorHandler:

End Sub

Private Sub mnuFileSave_Click()
    On Error GoTo errorHandler
    
    Dim i As Integer, j As Integer
    Dim address As Long
    Dim INT8 As Byte
    Dim INT16 As Integer

    If tsbFileName = "" Then
        mnuFileSave.Enabled = False
        Exit Sub
    End If
    
    tsbFile = FreeFile
    Open tsbFileName For Binary As tsbFile
    
    Seek #tsbFile, &H329A7 + 1
    
    address = 35275 '&H89CB
    
    For i = 0 To 16
        If address > 32767 Then
            INT16 = (address - 65536)
        Else
            INT16 = address
        End If
        
        Put #tsbFile, , INT16
        address = address + Schedule.Week(i).gameCount * 2
    Next i
    
    For i = 0 To 16
        INT8 = Schedule.Week(i).gameCount
        Put #tsbFile, , INT8
    Next
    INT8 = &HFF
    Put #tsbFile, , INT8
    
    For i = 0 To 16
        For j = 0 To Schedule.Week(i).gameCount - 1
            INT8 = Schedule.Week(i).Matchup(j).Visitor
            Put #tsbFile, , INT8
            INT8 = Schedule.Week(i).Matchup(j).Home
            Put #tsbFile, , INT8
        Next j
    Next
    
    Close tsbFile
    
    Exit Sub
    
errorHandler:
    Close tsbFile
    MsgBox "Something went wrong during the save.", vbCritical + vbOKOnly, "Error"
End Sub

Private Sub mnuHelpAbout_Click()
    MsgBox "TSB Schedule Editor" & vbCrLf & vbCrLf & "�2004 by Matthew Leverton" & vbCrLf & vbCrLf & "https://bitbucket.org/leverton/tsb-nes-schedule-editor", vbInformation, "About"
End Sub

Private Sub udWeek_DownClick()
    If weekNumber = 0 Then
        weekNumber = 16
    Else
        weekNumber = weekNumber - 1
    End If
    Call LoadWeek(weekNumber)
End Sub

Private Sub udWeek_UpClick()
    weekNumber = weekNumber + 1
    If weekNumber > 16 Then weekNumber = 0
    Call LoadWeek(weekNumber)
End Sub

Private Sub upGames_UpClick()
    If Schedule.Week(weekNumber).gameCount < 14 Then
        Schedule.Week(weekNumber).gameCount = Schedule.Week(weekNumber).gameCount + 1
        Call showMatchups
    End If
End Sub

Private Sub upGames_DownClick()
    If Schedule.Week(weekNumber).gameCount > 1 Then
        Schedule.Week(weekNumber).gameCount = Schedule.Week(weekNumber).gameCount - 1
        Call showMatchups
    End If
End Sub


Private Sub showMatchups()
    Dim i As Integer
    
    For i = 0 To 13
        If i < Schedule.Week(weekNumber).gameCount Then
            cmbTeam(i * 2).Visible = True
            cmbTeam(i * 2 + 1).Visible = True
            lblAt(i).Visible = True
        Else
            cmbTeam(i * 2).Visible = False
            cmbTeam(i * 2 + 1).Visible = False
            lblAt(i).Visible = False
        End If
    Next i
    
    lblGames.Caption = "Games: " & Schedule.Week(weekNumber).gameCount
End Sub

Function openFile(fileName As String)
    Dim INT16 As Integer
    Dim address As Long
    Dim gameCount As Byte
    Dim i As Integer, j As Integer
    
    weekNumber = 0
    
    ' Schedule Starts at 0x329DB in NES rom
    ' Internal address is 0x89CB
    ' with a difference of 0x2A010

    tsbFile = FreeFile
    Open fileName For Binary As tsbFile
    
    ' Load Team abbreviations
    Seek #tsbFile, &H1FD00 + 1
    For i = 0 To 29
        Team(i).Abbr = readString(tsbFile, 4)
    Next
    
    ' Load Schedule
    For i = 0 To 16
        address = readUINT16(tsbFile, &H329A7 + i * 2)
        gameCount = readINT8(tsbFile, &H329C9 + i)
        
        If gameCount < 0 Then gameCount = 0
        If gameCount > 14 Then gameCount = 14
        
        Schedule.Week(i).gameCount = gameCount
        Seek #tsbFile, address + &H2A010 + 1
                
        For j = 0 To gameCount - 1
            Schedule.Week(i).Matchup(j).Visitor = readINT8(tsbFile)
            Schedule.Week(i).Matchup(j).Home = readINT8(tsbFile)
            
            If Schedule.Week(i).Matchup(j).Visitor > 29 Then Schedule.Week(i).Matchup(j).Visitor = 0
            If Schedule.Week(i).Matchup(j).Home > 29 Then Schedule.Week(i).Matchup(j).Home = 0
        Next j
    Next i
    
    Close #tsbFile
    
    For i = 0 To 27
        cmbTeam(i).Clear
        For j = 0 To 27
            cmbTeam(i).AddItem Team(j).Abbr
        Next j
        cmbTeam(i).Text = ""
    Next i
    
    Call LoadWeek(weekNumber)
    
    StatusBar.Panels(1).Text = fileName
    frmContent.Visible = True

End Function

